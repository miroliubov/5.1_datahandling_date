package src.demo;

import src.service.DateService;

public class DemoService implements IDemoService {

    @Override
    public void execute() {
        IDateService dateService = new DateService();
        System.out.println("Задача 1.1");
        dateService.dateBetweenTwoDates(1998, 1,12, 3, 27,39);
        System.out.println("Задача 1.2");
        dateService.daysBetweenTwoDates("25.02.1972", "16.11.2001");
        System.out.println("Задача 1.3");
        dateService.convertStringIntoDate("Wednesday, Aug 10, 2016 12:10:56 PM");
        System.out.println("Задача 1.3 с ижевским временем");
        dateService.izhTime("2016-08-16T10:15:30+08:00");
    }
}
