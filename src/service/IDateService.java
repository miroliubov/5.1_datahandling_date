package src.service;

public interface IDateService {

    void dateBetweenTwoDates(int year, int months, int day, int hour, int min, int sec);
    void daysBetweenTwoDates(String date1, String date2);
    void convertStringIntoDate(String date);
    void izhTime(String date);

}
