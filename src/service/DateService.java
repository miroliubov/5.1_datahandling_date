package src.service;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

public class DateService implements IDateService {

    @Override
    public void dateBetweenTwoDates(int year, int months, int day, int hour, int min, int sec) {
        LocalDateTime dateOfBirth = LocalDateTime.of(year, months, day, hour, min, sec);
        System.out.println("Лет с рождения: " + ChronoUnit.YEARS.between(dateOfBirth ,LocalDateTime.now()));
        System.out.println("Месяцев с рождения: " + ChronoUnit.MONTHS.between(dateOfBirth ,LocalDateTime.now()));
        System.out.println("Дней с рождения: " + ChronoUnit.DAYS.between(dateOfBirth ,LocalDateTime.now()));
        System.out.println("Часов с рождения: " + ChronoUnit.HOURS.between(dateOfBirth ,LocalDateTime.now()));
        System.out.println("Минут с рождения: " + ChronoUnit.MINUTES.between(dateOfBirth ,LocalDateTime.now()));
        System.out.println("Секунд с рождения: " + ChronoUnit.SECONDS.between(dateOfBirth ,LocalDateTime.now()));
    }

    @Override
    public void daysBetweenTwoDates(String date1, String date2) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate firstDate = LocalDate.parse(date1, formatter);
        LocalDate secondDate = LocalDate.parse(date2, formatter);
        if (firstDate.isAfter(secondDate)) {
            System.out.println("Дней между датами: " + ChronoUnit.DAYS.between(secondDate, firstDate));
        }
        else if (firstDate.isBefore(secondDate)) {
            System.out.println("Дней между датами: " + ChronoUnit.DAYS.between(firstDate ,secondDate));
        }
        else {
            System.out.println("Дней между датами: 0");
        }
    }

    @Override
    public void convertStringIntoDate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEEE, MMM d, yyyy hh:mm:ss a", Locale.ENGLISH);
        LocalDate formatedDate = LocalDate.parse(date, formatter);
        System.out.println(formatedDate);
    }

    @Override
    public  void izhTime(String date) {
        ZonedDateTime dateIzh = ZonedDateTime.parse(date).withZoneSameInstant(ZoneId.of("Europe/Samara"));
        DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
        System.out.println(dateIzh.format(formatter));
    }

}
